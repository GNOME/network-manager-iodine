# Portuguese translation for network-manager-iodine.
# Copyright (C) 2015 network-manager-iodine's COPYRIGHT HOLDER
# This file is distributed under the same license as the network-manager-iodine package.
# Tiago S. <almosthumane@portugalmail.pt>, 2015.
# Pedro Albuquerque <palbuquerque73@gmail.com>, 2015.
# Hugo Carvalho <hugokarvalho@hotmail.com>, 2021, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: network-manager-iodine master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/network-manager-iodine/"
"issues\n"
"POT-Creation-Date: 2020-07-15 14:34+0000\n"
"PO-Revision-Date: 2022-01-04 14:26+0000\n"
"Last-Translator: Hugo Carvalho <hugokarvalho@hotmail.com>\n"
"Language-Team: \n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.0.1\n"
"X-Project-Style: gnome\n"

#: ../appdata/network-manager-iodine.metainfo.xml.in.h:1
#: ../properties/nm-iodine.c:60
msgid "Iodine DNS Tunnel"
msgstr "Túnel DNS Iodine"

#: ../appdata/network-manager-iodine.metainfo.xml.in.h:2
msgid "Client for Iodine DNS Tunnel"
msgstr "Cliente para Túnel DNS Iodine"

#: ../appdata/network-manager-iodine.metainfo.xml.in.h:3
msgid "Support for tunnelling IP traffic via DNS using Iodine."
msgstr "Suporte para túnel de tráfego IP via DNS usando Iodine."

#: ../appdata/network-manager-iodine.metainfo.xml.in.h:4
msgid ""
"This is useful to get useful connectivity in cases where Internet access is "
"severely filtered."
msgstr ""
"Isto é útil para obter ligação útil nos casos em que o acesso à Internet é "
"severamente filtrado."

#: ../appdata/network-manager-iodine.metainfo.xml.in.h:5
msgid "Guido Günther"
msgstr "Guido Günther"

#: ../auth-dialog/main.c:169
#, c-format
msgid "You need to authenticate to access the Virtual Private Network “%s”."
msgstr "Tem de se autenticar para aceder à rede privada virtual(VPN) “%s”."

#: ../auth-dialog/main.c:182 ../auth-dialog/main.c:205
msgid "Authenticate VPN"
msgstr "Autenticação VPN"

#: ../auth-dialog/main.c:185
msgid "Password:"
msgstr "Senha:"

#: ../properties/nm-iodine.c:61
msgid "Tunnel connections via DNS."
msgstr "Ligações em túnel via DNS."

#: ../properties/nm-iodine-dialog.ui.h:1
msgid "Saved"
msgstr "Gravado"

#: ../properties/nm-iodine-dialog.ui.h:2
msgid "Always ask"
msgstr "Perguntar sempre"

#: ../properties/nm-iodine-dialog.ui.h:3
msgid "General"
msgstr "Geral"

#: ../properties/nm-iodine-dialog.ui.h:4
msgid "_Toplevel Domain:"
msgstr "Domínio de _Topo:"

#: ../properties/nm-iodine-dialog.ui.h:5
msgid "Optional"
msgstr "Opcional"

#: ../properties/nm-iodine-dialog.ui.h:6
msgid "_Nameserver:"
msgstr "Servidor de _Nomes:"

#: ../properties/nm-iodine-dialog.ui.h:7
msgid "_Password:"
msgstr "_Senha:"

#: ../properties/nm-iodine-dialog.ui.h:8
msgid "_Fragment Size:"
msgstr "Tamanho do _Fragmento:"

#: ../properties/nm-iodine-dialog.ui.h:9
msgid "Show password"
msgstr "Mostrar senha"

#: ../src/nm-iodine-service.c:133
#, c-format
msgid "invalid integer property “%s” or out of range [%d -> %d]"
msgstr "propriedade integer \"%s\" inválida ou fora dos limites [%d -> %d]"

#: ../src/nm-iodine-service.c:144
#, c-format
msgid "invalid boolean property “%s” (not yes or no)"
msgstr "propriedade boolean \"%s\" inválida (não é sim ou não)"

#: ../src/nm-iodine-service.c:151
#, c-format
msgid "unhandled property “%s” type %s"
msgstr "propriedade não gerida \"%s\" tipo %s"

#: ../src/nm-iodine-service.c:165
#, c-format
msgid "property “%s” invalid or not supported"
msgstr "propriedade \"%s\" inválida ou não suportada"

#: ../src/nm-iodine-service.c:181
msgid "No VPN configuration options."
msgstr "Sem opções de configuração VPN."

#: ../src/nm-iodine-service.c:200
msgid "No VPN secrets!"
msgstr "Sem segredos VPN!"

#: ../src/nm-iodine-service.c:494
msgid "Could not find iodine binary."
msgstr "Impossível encontrar um binário iodine."
