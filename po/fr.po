# French translation for network-manager-iodine.
# Copyright (C) 2014 network-manager-iodine's COPYRIGHT HOLDER
# This file is distributed under the same license as the network-manager-iodine package.
# Michael Scherer <misc@zarb.org>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: network-manager-iodine master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/network-manager-iodine/"
"issues\n"
"POT-Creation-Date: 2018-11-04 23:39+0000\n"
"PO-Revision-Date: 2019-05-23 12:39+0200\n"
"Last-Translator: Michael Scherer <misc@zarb.org>\n"
"Language-Team: French <gnomefr@traduc.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Poedit 1.5.4\n"

#: ../appdata/network-manager-iodine.metainfo.xml.in.h:1
#: ../properties/nm-iodine.c:60
msgid "Iodine DNS Tunnel"
msgstr "Tunnel DNS avec Iodine"

#: ../appdata/network-manager-iodine.metainfo.xml.in.h:2
msgid "Client for Iodine DNS Tunnel"
msgstr "Client pour les tunnels DNS avec Iodine"

#: ../appdata/network-manager-iodine.metainfo.xml.in.h:3
msgid "Support for tunnelling IP traffic via DNS using Iodine."
msgstr ""
"Prise en charge des tunnels pour le trafic IP via DNS à l’aide d’Iodine."

#: ../appdata/network-manager-iodine.metainfo.xml.in.h:4
msgid ""
"This is useful to get useful connectivity in cases where Internet access is "
"severely filtered."
msgstr ""
"Cela permet d’obtenir une connectivité utile lorsque l’accès à Internet est "
"largement filtré."

#: ../appdata/network-manager-iodine.metainfo.xml.in.h:5
msgid "Guido Günther"
msgstr "Guido Günther"

#: ../auth-dialog/main.c:169
#, c-format
msgid "You need to authenticate to access the Virtual Private Network “%s”."
msgstr "Authentification requise pour accéder au VPN « %s »."

#: ../auth-dialog/main.c:182 ../auth-dialog/main.c:205
msgid "Authenticate VPN"
msgstr "Authentification du VPN"

#: ../auth-dialog/main.c:185
msgid "Password:"
msgstr "Mot de passe :"

#: ../properties/nm-iodine.c:61
msgid "Tunnel connections via DNS."
msgstr "Connexions par tunnel via DNS."

#: ../properties/nm-iodine-dialog.ui.h:1
msgid "Saved"
msgstr "Enregistré"

#: ../properties/nm-iodine-dialog.ui.h:2
msgid "Always ask"
msgstr "Toujours demander"

#: ../properties/nm-iodine-dialog.ui.h:3
msgid "General"
msgstr "Général"

#: ../properties/nm-iodine-dialog.ui.h:4
msgid "_Toplevel Domain:"
msgstr "Domaine DNS du _tunnel :"

#: ../properties/nm-iodine-dialog.ui.h:5
msgid "Optional"
msgstr "Facultatif"

#: ../properties/nm-iodine-dialog.ui.h:6
msgid "_Nameserver:"
msgstr "Serveur de _noms :"

#: ../properties/nm-iodine-dialog.ui.h:7
msgid "_Password:"
msgstr "Mot de _passe :"

#: ../properties/nm-iodine-dialog.ui.h:8
msgid "_Fragment Size:"
msgstr "Taille des _fragments :"

#: ../properties/nm-iodine-dialog.ui.h:9
msgid "Show password"
msgstr "Afficher le mot de passe"

#: ../src/nm-iodine-service.c:133
#, c-format
msgid "invalid integer property “%s” or out of range [%d -> %d]"
msgstr ""
"propriété numérique entière « %s » invalide ou en dehors de l'intervalle [%d "
"-> %d]"

#: ../src/nm-iodine-service.c:144
#, c-format
msgid "invalid boolean property “%s” (not yes or no)"
msgstr ""
"propriété booléenne « %s » invalide (doit être « yes » pour oui ou « no » "
"pour non)"

#: ../src/nm-iodine-service.c:151
#, c-format
msgid "unhandled property “%s” type %s"
msgstr "propriété « %s » de type « %s » non gérée"

#: ../src/nm-iodine-service.c:165
#, c-format
msgid "property “%s” invalid or not supported"
msgstr "propriété « %s » invalide ou non prise en charge"

#: ../src/nm-iodine-service.c:181
msgid "No VPN configuration options."
msgstr "Aucune option de configuration du VPN."

#: ../src/nm-iodine-service.c:200
msgid "No VPN secrets!"
msgstr "Aucun secret pour le VPN."

#: ../src/nm-iodine-service.c:494
msgid "Could not find iodine binary."
msgstr "Impossible de trouver le programme iodine."
